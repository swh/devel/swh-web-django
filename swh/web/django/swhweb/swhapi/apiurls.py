import re

from django.conf.urls import url
from rest_framework.decorators import api_view

swh_api_version = '1'


def gen_api_url(api_endpoint):
    return '^' + swh_api_version + api_endpoint + '$'


class APIUrls(object):
    """
    Class to manage API documentation URLs.
      * Indexes all routes documented using apidoc's decorators.
      * Tracks endpoint/request processing method relationships for use
        in generating related urls in API documentation
    Relies on the load_controllers logic in main.py for initialization.

    """
    apidoc_routes = {}
    method_endpoints = {}
    urlpatterns = []

    @classmethod
    def get_app_endpoints(cls):
        return cls.apidoc_routes

    @classmethod
    def get_method_endpoints(cls, f):
        if f.__name__ not in cls.method_endpoints:
            cls.method_endpoints[f.__name__] = cls.group_routes_by_method(f)
        return cls.method_endpoints[f.__name__]

    @classmethod
    def group_routes_by_method(cls, f):
        """
        Group URL endpoints according to their processing method.
        Returns:
            A dict where keys are the processing method names, and values
            are the routes that are bound to the key method.
        """
        rules = []
        for urlp in cls.urlpatterns:
            endpoint = urlp.callback.__name__
            if endpoint != f.__name__:
                continue
            method_names = urlp.callback.view_class.http_method_names
            url_rule = urlp.regex.pattern.replace('^', '/').replace('$', '')
            url_rule_params = re.findall('\([^)]+\)', url_rule)
            for param in url_rule_params:
                param_name = re.findall('<(.*)>', param)
                param_name = param_name[0] if len(param_name) > 0 else None
                if param_name:
                    param_index = \
                        next(i for (i, d) in enumerate(f.doc_data['args'])
                             if d['name'] == param_name)
                    if param_index is not None:
                        url_rule = url_rule.replace(
                            param, '<' +
                            f.doc_data['args'][param_index]['name'] +
                            ': ' + f.doc_data['args'][param_index]['type'] +
                            '>')
            rule_dict = {'rule': '/api' + url_rule,
                         'name': urlp.name,
                         'methods': {method.upper() for method in method_names}
                         }
            rules.append(rule_dict)

        return rules

    @classmethod
    def index_add_route(cls, route, docstring, **kwargs):
        """
        Add a route to the self-documenting API reference
        """
        route_view_name = route[1:-1].replace('/', '-')
        if route not in cls.apidoc_routes:
            d = {'docstring': docstring,
                 'route_view_name': route_view_name}
            for k, v in kwargs.items():
                d[k] = v
            cls.apidoc_routes[route] = d

    @classmethod
    def index_add_url_pattern(cls, url_pattern, view, view_name):
        cls.urlpatterns.append(url(gen_api_url(url_pattern), view,
                                   name=view_name))

    @classmethod
    def get_url_patterns(cls):
        return cls.urlpatterns


class api_route(object):  # noqa: N801
    """
    """

    api_views = {}

    def __init__(self, url_pattern=None, view_name=None, 
                 methods=['GET', 'HEAD']):
        super().__init__()
        self.url_pattern = url_pattern
        self.view_name = view_name
        self.methods = methods

    def __call__(self, f):

        @api_view(self.methods)
        def api_view_f(*args, **kwargs):
            return f(*args, **kwargs)
        api_view_f.__name__ = f.__name__

        APIUrls.index_add_url_pattern(self.url_pattern, api_view_f,
                                      self.view_name)
        return f
