#!/usr/bin/env python

# Copyright (C) 2017  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
import sys

from swhweb import config

# Default configuration file
DEFAULT_CONF_FILE = '~/.config/swh/webapp.yml'

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "swhweb.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    config.read_config(DEFAULT_CONF_FILE)
    execute_from_command_line(sys.argv)
